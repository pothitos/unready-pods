# Unready Kubernetes pods

You may invoke the script via
```sh
watch -c ./print_unready_pods.sh <namespace1> [<namespace2> [...]]
```
