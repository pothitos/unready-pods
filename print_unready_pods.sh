#!/bin/bash -e
# https://github.com/kubernetes/kubernetes/issues/49387

TITLE_START="\e[1;32m"
TITLE_END="\e[0m"

for NAMESPACE in "$@"
do
  TOTAL_PODS=$(kubectl get pods -n "$NAMESPACE" --no-headers 2> /dev/null | wc -l)
  echo -e "$TITLE_START$NAMESPACE$TITLE_END has $TOTAL_PODS pods"
  kubectl get pods -n "$NAMESPACE" |
    gawk '{if (!(match($2, /([0-9]+)\/([0-9]+)/, c) && c[1] == c[2]) &&
               $3 != "Completed") print}'
  echo
done
